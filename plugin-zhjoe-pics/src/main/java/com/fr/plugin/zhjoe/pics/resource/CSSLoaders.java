package com.fr.plugin.zhjoe.pics.resource;

import com.fr.stable.fun.impl.AbstractCssFileHandler;

public class CSSLoaders extends AbstractCssFileHandler {
	@Override
	public String[] pathsForFiles() {
		return new String[] { "com/fr/plugin/zhjoe/pics/web/swiper.min.css","com/fr/plugin/zhjoe/pics/web/index.css" };
	}
}
