package com.fr.plugin.zhjoe.http;

import com.fr.decision.fun.HttpHandler;
import com.fr.decision.fun.impl.AbstractHttpHandlerProvider;
import com.fr.plugin.zhjoe.http.service.HttpGetMd5;
import com.fr.plugin.zhjoe.http.service.SSOService;

public class HttpProvider extends AbstractHttpHandlerProvider {
	@Override
	public HttpHandler[] registerHandlers() {
		return new HttpHandler[]{new HttpGetMd5(),new SSOService()};
	}
}
