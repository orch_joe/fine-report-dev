package com.fr.plugin.zhjoe.tools;

import com.fr.base.Formula;
import com.fr.general.GeneralUtils;
import com.fr.intelli.record.Focus;
import com.fr.intelli.record.Original;
import com.fr.script.AbstractFunction;
import com.fr.stable.Primitive;
import com.fr.stable.ProductConstants;
import com.fr.stable.exception.FormulaException;
import com.fr.stable.web.SessionProvider;
import com.fr.web.core.SessionPoolManager;
import com.fr.web.session.SessionLocalManager;

import java.lang.reflect.Method;

public class IP extends AbstractFunction {
	@Override
	@Focus(id = "com.fr.plugin.zhjoe", text = "ZHJOE_TOOLS_IP", source = Original.PLUGIN)
	public Object run(Object[] args) throws FormulaException {
		SessionProvider info = SessionLocalManager.getSession();
		//		String userName = info.getWebContext().getUserName();
		//		System.out.println(userName);
		//		System.out.println(info.getWebContext().getTitle());
		//		System.out.println(info.getWebContext().getUserRole());


		try {
			Object localObject = getCalculator().eval(new Formula("=$sessionID"));
			String str = GeneralUtils.objectToString(localObject);
			System.out.println(str);



			return findUserIP(str);

		} catch (Exception utilEvalError) {
			return "";
		}
	}

	private Object findUserIP(String paramString) {
		SessionProvider provider = SessionPoolManager.getSessionIDInfor(paramString,SessionProvider.class);
		return provider.getWebContext().getAddress();
	}
}
