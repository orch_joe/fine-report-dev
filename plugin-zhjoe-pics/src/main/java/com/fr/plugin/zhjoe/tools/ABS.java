package com.fr.plugin.zhjoe.tools;

import com.fr.general.FArray;
import com.fr.general.GeneralUtils;
import com.fr.intelli.record.Focus;
import com.fr.intelli.record.Original;
import com.fr.record.analyzer.EnableMetrics;
import com.fr.script.AbstractFunction;
import com.fr.stable.ArrayUtils;
import com.fr.stable.Primitive;

@EnableMetrics
public class ABS extends AbstractFunction {

	@Focus(id = "com.fr.plugin.zhjoe", text = "ZHJOE_TOOLS_ABS", source = Original.PLUGIN)
	public Object run(Object[] args) {
		int len = ArrayUtils.getLength(args);
		if (len == 0) {
			return Primitive.ERROR_VALUE;
		} else if (len == 1) {
			Object one = args[0];
			if (one instanceof FArray) {
				FArray data = (FArray) one;
				FArray<Double> result = new FArray();
				for (Object el : data) {
					result.add(Math.abs(GeneralUtils.objectToNumber(el).doubleValue()));
				}
				return result;
			} else {
				return Math.abs(GeneralUtils.objectToNumber(one).doubleValue());
			}
		} else {
			FArray<Double> result = new FArray();
			for (Object arg : args) {
				result.add(Math.abs(GeneralUtils.objectToNumber(arg).doubleValue()));
			}
			return result;
		}
	}
}
