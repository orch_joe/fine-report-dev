package com.fr.plugin.zhjoe.tools;

import com.fr.base.Formula;
import com.fr.general.GeneralUtils;
import com.fr.stable.web.SessionProvider;
import com.fr.web.core.SessionPoolManager;

public class SessionUtil {
	public static SessionProvider getSessionProvider(String sessionId){
		System.out.println(sessionId);
		SessionProvider provider = SessionPoolManager.getSessionIDInfor(sessionId,SessionProvider.class);
		return provider;
	}
}
