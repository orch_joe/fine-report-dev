package com.fr.plugin.zhjoe.db;

import com.fr.plugin.zhjoe.util.StringUtil;
import com.fr.stable.web.SessionProvider;
import com.fr.third.guava.collect.Lists;
import com.fr.web.session.SessionLocalManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProjectDbUtil {
	public static List<Map<String, Object>> getProject(String url, String name, String password, String sql) {
		System.out.println("url:" + url);
		System.out.println("name:" + name);
		System.out.println("password:" + password);
		System.out.println("sql:" + sql);
		List<Map<String, Object>> results = new ArrayList();
		Connection connection = DBUtil.getconnection(url, name, password);
		PreparedStatement preparedStatement;
		ResultSet result = null;
		Statement statement = null;
		try {
			preparedStatement = connection.prepareStatement(sql);
			result = preparedStatement.executeQuery();
			while (result.next()) {
				Map<String, Object> map = new HashMap();
				map.put("id", result.getString(1));
				map.put("url", result.getString(2));
				map.put("msg", result.getString(3));
				results.add(map);
			}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		} finally {
			DBUtil.close(result, statement, connection);
		}
		return results;
	}
}
