package com.fr.plugin.zhjoe.pics.resource;

import com.fr.stable.fun.impl.AbstractJavaScriptFileHandler;

public class JSFileLoaders extends AbstractJavaScriptFileHandler {
	@Override
	public String[] pathsForFiles() {
		return new String[]{"/com/fr/plugin/zhjoe/pics/web/swiper.min.js","/com/fr/plugin/zhjoe/pics/web/pictures.js"};
	}
}
