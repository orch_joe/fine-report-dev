package com.fr.plugin.zhjoe.pics.ui;

import com.fr.design.mainframe.widget.accessibles.AccessiblePropertyEditor;

public class PictureslEditor extends AccessiblePropertyEditor
{
  public PictureslEditor(Object paramObject)
  {
    super(new AccessiblePicturesModelEditor((PicturesWidget)paramObject));
  }
}
