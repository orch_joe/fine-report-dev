package com.fr.plugin.zhjoe.tools;

import com.fr.intelli.record.Focus;
import com.fr.intelli.record.Original;
import com.fr.plugin.zhjoe.util.MD5Util;
import com.fr.script.AbstractFunction;
import com.fr.stable.Primitive;
import com.fr.stable.exception.FormulaException;

public class MD5 extends AbstractFunction {

	@Override
	@Focus(id = "com.fr.plugin.zhjoe.pics", text = "ZHJOE_TOOLS_MD5", source = Original.PLUGIN)
	public Object run(Object[] objects) throws FormulaException {
		if (null != objects && objects.length == 1) {
			String str = String.valueOf(objects);
			return MD5Util.MD5(str);
		} else {
			return Primitive.ERROR_VALUE;
		}
	}
}
