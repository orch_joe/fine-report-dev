package com.fr.plugin.zhjoe.pics;

import com.fr.design.fun.impl.AbstractFormWidgetOptionProvider;
import com.fr.form.ui.Widget;
import com.fr.plugin.zhjoe.pics.ui.PicturesWidget;
import com.fr.plugin.zhjoe.pics.ui.XPictures;

public class PicturesImpl extends AbstractFormWidgetOptionProvider {
	public Class<? extends Widget> classForWidget() {
		return PicturesWidget.class;
	}

	public Class<?> appearanceForWidget() {
		return XPictures.class;
	}

	public String iconPathForWidget() {
		return "com/fr/plugin/zhjoe/pics/web/icons.png";
	}

	public String nameForWidget() {
		return "PICS GOGO";
	}
}
