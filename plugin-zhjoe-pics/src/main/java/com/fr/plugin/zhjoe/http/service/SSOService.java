package com.fr.plugin.zhjoe.http.service;

import com.fr.decision.fun.impl.BaseHttpHandler;
import com.fr.decision.webservice.v10.login.LoginService;
import com.fr.log.FineLoggerFactory;
import com.fr.plugin.zhjoe.util.MD5Util;
import com.fr.third.springframework.web.bind.annotation.RequestMethod;
import com.fr.web.utils.WebUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

public class SSOService extends BaseHttpHandler {

	@Override
	public RequestMethod getMethod() {
		return RequestMethod.GET;
	}

	@Override
	public String getPath() {
		return "/ssoService";
	}

	@Override
	public boolean isPublic() {
		return true;
	}

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		FineLoggerFactory.getLogger().info("开始进入filter......");
		String userName = WebUtils.getHTTPRequestParameter(req, "userName");
		String salt = WebUtils.getHTTPRequestParameter(req, "salt");
		String secret = WebUtils.getHTTPRequestParameter(req, "secret");
		try {
			FineLoggerFactory.getLogger().error("username:" + userName);
			System.out.println("code:" + userName);
			System.out.println("salt:" + salt);
			System.out.println("secret:" + secret);
			String pass = MD5Util.MD5(userName+".123."+salt);
			if (pass.equalsIgnoreCase(secret)) {
				String token = LoginService.getInstance().login(req, res, userName);
				req.setAttribute("fine_auth_token", token);
				res.sendRedirect("/decision");
			}else {
				PrintWriter out = res.getWriter();
				out.println("认证失败");
				out.flush();
				out.close();
			}
		} catch (Exception e) {
			FineLoggerFactory.getLogger().error(e.getMessage(), e);
		}
	}
}
