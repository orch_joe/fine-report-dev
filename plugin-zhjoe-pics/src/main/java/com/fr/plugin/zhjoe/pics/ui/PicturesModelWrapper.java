package com.fr.plugin.zhjoe.pics.ui;

import com.fr.design.Exception.ValidationException;
import com.fr.design.designer.properties.Decoder;
import com.fr.design.designer.properties.Encoder;
import com.fr.report.cell.FloatElement;

public class PicturesModelWrapper implements Encoder<FloatElement>, Decoder<FloatElement> {
	public FloatElement decode(String paramString) {
		return null;
	}

	public void validate(String paramString) throws ValidationException {
	}

	public String encode(FloatElement paramFloatElement) {
		if (paramFloatElement != null)
			return paramFloatElement.getName();
		return null;
	}
}
