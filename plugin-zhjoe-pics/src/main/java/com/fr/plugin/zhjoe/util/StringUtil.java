package com.fr.plugin.zhjoe.util;

import java.util.*;

public class StringUtil {
	public static boolean isNull(Object object) {
		return object == null;
	}

	/**
	 * * 判断一个对象是否非空
	 *
	 * @param object Object
	 * @return true：非空 false：空
	 */
	public static boolean isNotNull(Object object) {
		return !isNull(object);
	}

	public static boolean isNotEmpty(String str) {
		if (null != str && str.length() > 0 && !"null".equals(str)) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean equalsNotNull(String str1, String str2) {
		if (null == str1 || null == str1) {
			return false;
		} else {
			if (str1.equals(str2)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static boolean isEmpty(String str) {
		if (null != str && str.trim().length() > 0 && !"null".equals(str) && !"undefined".equals(str)) {
			return false;
		} else {
			return true;
		}
	}


	public static String getEncoding(String str) {
		if (isEmpty(str)) {
			return str;
		}
		String[] encodeArr = new String[]{"ISO-8859-1", "GB2312", "GBK", "UTF-8", "UTF-16"};
		try {
			for (String encode : encodeArr) {
				if (str.equals(new String(str.getBytes(encode), encode))) {
					return encode;
				}
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	public static List<String> array2List(String[] arr) {
		return Arrays.asList(arr);
	}

	public static String[] list2Array(List<String> list) {
		String[] array = new String[list.size()];
		String[] s = list.toArray(array);
		return s;
	}

	/**
	 * 将一个字符串数组拼为一个带分隔符的字符串
	 * @param array
	 * @return
	 */
	public static String buildSplitString(String[] array, String split) {
		StringBuffer str = new StringBuffer();
		if (null != array && 0 != array.length && null != split) {
			for (int i = 0; i < array.length; i++) {
				str.append(array[i]);
				str.append(split);
			}
			str = new StringBuffer(str.substring(0, str.length() - split.length()));
		}
		return str.toString();

	}

	/**
	 * 将一个字符串数组拼为一个带分隔符的字符串
	 *
	 * @param list
	 * @return
	 */
	public static String buildSplitString(Collection<String> list, String split) {
		if (null != list && 0 != list.size() && null != split) {
			return buildSplitString(list.toArray(new String[list.size()]), split);
		} else {
			return "";
		}
	}

	/**
	 * 根据数据对象得到一个可执行sql条件查询的字符串
	 * @param array
	 * @return
	 */
	public static String buildArrayToSQLString(String[] array) {
		StringBuffer str = new StringBuffer("");
		if (null != array && 0 != array.length) {
			for (int i = 0; i < array.length; i++) {
				if (isNotEmpty(array[i])) {
					str.append("'");
					str.append(array[i]);
					str.append("',");
				} else {
					str.append("'',");
				}
			}
			str.deleteCharAt(str.length() - 1);
		} else {
			str.append("''");
		}
		return str.toString();

	}

	/***
	 * 传入任意长度的字符数组，将其"去重"后拼为一个字符串数组
	 * chenmeng
	 * 2012-1-6 下午02:20:14
	 * @param array
	 */
	public static String[] mergeStringArray(String[]... array) {
		Set<String> arraySet = new HashSet();
		String[] newArray = null;
		for (int i = 0; i < array.length; i++) {
			String[] a1 = array[i];
			for (int j = 0; j < a1.length; j++) {
				arraySet.add(a1[j]);
			}
		}
		newArray = new String[arraySet.size()];
		arraySet.toArray(newArray);
		return newArray;
	}

	public static String getUUID() {
		return UUID.randomUUID().toString().replace("-", "").toLowerCase();
	}

	public static String getUUID(int length) {
		String uuid = UUID.randomUUID().toString().replace("-", "").toLowerCase();
		if (length <= 3) {
			length = 3;
		}
		if (length > 20) {
			length = 20;
		}
		return uuid.substring(0, length);
	}

	public static String getUUID(boolean replace) {
		if (replace) {
			return getUUID();
		} else {
			return UUID.randomUUID().toString().toLowerCase();
		}
	}

	public static String getDefaultValue(Object value, String defaultValue) {
		if (null == value) {
			return defaultValue;
		} else {
			return String.valueOf(value);
		}
	}

	public static String getDefaultValue(String value, String defaultValue) {
		if (isEmpty(value)) {
			return defaultValue;
		} else {
			return value;
		}
	}

	public static String getDefaultValue(String value) {
		return getDefaultValue(value, "");
	}

	public static void main(String[] args) {
		for (int i = 0; i < 10; i++) {
			System.out.println(getUUID());
		}
	}

	public static String buildOid() {
		return getUUID();
	}
}
