package com.fr.plugin.zhjoe.pics.ui;

import com.fr.base.FRContext;
import com.fr.base.Formula;
import com.fr.base.TemplateUtils;
import com.fr.general.GeneralUtils;
import com.fr.script.Calculator;
import com.fr.stable.StableUtils;
import com.fr.stable.UtilEvalError;
import com.fr.stable.web.Repository;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLable;
import com.fr.stable.xml.XMLableReader;

public class FormualXml implements XMLable {
	private String value;

	public String getValue(Repository paramRepository, Calculator paramCalculator) {
		if (StableUtils.canBeFormula(this.value))
			try {
				return GeneralUtils.objectToString(paramCalculator.eval(new Formula(this.value)));
			} catch (UtilEvalError localUtilEvalError) {
				FRContext.getLogger().error(localUtilEvalError.getMessage(), localUtilEvalError);
			}
		try {
			return TemplateUtils.render(this.value, paramCalculator);
		} catch (Exception localException) {
			FRContext.getLogger().error(localException.getMessage(), localException);
		}
		return this.value;
	}

	public String toString() {
		return this.value;
	}

	public void readXML(XMLableReader paramXMLableReader) {
		if (paramXMLableReader.isChildNode()) {
			String str = paramXMLableReader.getTagName();
			if ("Attr".equals(str)) {
				this.value = paramXMLableReader.getAttrAsString("url", null);
			}
		}
	}

	public void writeXML(XMLPrintWriter paramXMLPrintWriter) {
		paramXMLPrintWriter.startTAG("Attr");
		paramXMLPrintWriter.attr("url", this.value);
		paramXMLPrintWriter.end();
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Object clone() throws CloneNotSupportedException {
		FormualXml localVideoRemoteSource = (FormualXml) super.clone();
		localVideoRemoteSource.value = this.value;
		return localVideoRemoteSource;
	}

}
