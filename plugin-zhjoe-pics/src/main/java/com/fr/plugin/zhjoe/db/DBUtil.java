package com.fr.plugin.zhjoe.db;

import java.sql.*;

public class DBUtil {
	/**
	 * 程序驱动
	 *
	 * @author 冯申然
	 */
	static {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 连接mysql数据库
	 *
	 * @author 冯申然
	 */
	public static Connection getconnection(String url, String name, String password) {
		try {
			return DriverManager.getConnection(url, name, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


//	/**
//	 * 数据的增删改查
//	 *
//	 * @author 冯申然
//	 */
//	public static boolean updata(String sql) {
//		Connection connection =null;
//		Statement statement =null;
//		try {
//			connection=getconnection();
//			statement = connection.createStatement();
//			int result = statement.executeUpdate(sql);
//			return result>0;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			close(statement,connection);
//		}
//		return false;
//	}
//
//	/**
//	 * 数据增删改查
//	 * @author 冯申然
//	 */
//	public static boolean updata(String sql,Object...params) {
//		Connection connection =null;
//		PreparedStatement prepareStatement=null;
//		try {
//			connection=getconnection();
//			prepareStatement = connection.prepareStatement(sql);
//
//			for(int i=1;i<=params.length;i++) {
//				prepareStatement.setObject(i, params[i-1]);
//			}
//			int result = prepareStatement.executeUpdate();
//			return result>0;
//		} catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			close(prepareStatement,connection);
//		}
//		return false;
//	}
//
//	/**
//	 * 登录设置
//	 *
//	 * @author 冯申然
//	 */
//	public static void select(String sql,IRowMapper rowMapper,Object...params) {//登录设置
//		ResultSet result =null;
//		Statement statement =null;
//		Connection connection =null;
//		PreparedStatement preparedStatement =null;
//		try {
//			connection=getconnection();
//			preparedStatement =connection.prepareStatement(sql);
//			for(int i=1;i<=params.length;i++) {
//				preparedStatement.setObject(i, params[i-1]);
//			}
//			result =preparedStatement.executeQuery();
//			rowMapper.rowMapper(result);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			close(result,statement,connection);
//		}
//	}
//
//	/**
//	 * 检验数据是否存在
//	 *
//	 * @author 冯申然
//	 */
//	public static boolean exist (String sql) {//是否存在
//		class RowMapper implements IRowMapper{
//			boolean state;
//			@Override
//			public void rowMapper(ResultSet result) {
//				try {
//					state=result.next();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		RowMapper rowMapper =new RowMapper();
//		select(sql,rowMapper);
//		return rowMapper.state;
//	}
//
//	/**
//	 * 检验数据是否存在
//	 *
//	 * @author 冯申然
//	 */
//	public static boolean exist (String sql,Object...params) {//是否存在
//		class RowMapper implements IRowMapper{
//			boolean state;
//			@Override
//			public void rowMapper(ResultSet result) {
//				try {
//					state=result.next();
//				} catch (SQLException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		RowMapper rowMapper =new RowMapper();
//		select(sql,rowMapper,params);
//		return rowMapper.state;
//	}
//
//	/**
//	 * 查询数据
//	 *
//	 * @author 冯申然
//	 */
//	public static void select(String sql,IRowMapper rowMapper) {//查询
//		Connection connection =null;
//		Statement statement =null;
//		ResultSet result =null;
//		try {
//			connection=getconnection();
//			statement = connection.createStatement();
//			result = statement.executeQuery(sql);
//			rowMapper.rowMapper(result);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			close(result,statement,connection);
//		}
//	}
//
//	/**
//	 * 查询数据
//	 *
//	 * @author 冯申然
//	 */
//	public static void Select(String sql,IRowMapper rowMapper,Object...params) {//查询
//		Connection connection =null;
//		Statement statement =null;
//		ResultSet result =null;
//		PreparedStatement preparedStatement =null;
//		try {
//			connection=getconnection();
//			preparedStatement =connection.prepareStatement(sql);
//			for(int i=1;i<=params.length;i++) {
//				preparedStatement.setObject(i, params[i-1]);
//			}
//			result =preparedStatement.executeQuery() ;
//			rowMapper.rowMapper(result);
//		} catch (Exception e) {
//			e.printStackTrace();
//		}finally {
//			close(result,statement,connection);
//		}
//	}

	public static void close(Statement statement, Connection connection) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public static void close(ResultSet result, Statement statement, Connection connection) {
		if (result != null) {
			try {
				result.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}
