package com.fr.plugin.zhjoe.pics.ui;

import com.fr.design.designer.creator.CRPropertyDescriptor;
import com.fr.design.designer.creator.XWidgetCreator;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.gui.itextfield.UITextField;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.mainframe.widget.editors.FormulaEditor;
import com.fr.design.mainframe.widget.editors.ShortCutTextEditor;
import com.fr.stable.ArrayUtils;

import javax.swing.*;
import java.awt.*;
import java.beans.IntrospectionException;

public class XPictures extends XWidgetCreator {
	public XPictures(PicturesWidget paramPicturesWidget, Dimension paramDimension) {
		super(paramPicturesWidget, paramDimension);
	}

	public String getIconPath() {
		return "com/fr/plugin/zhjoe/pics/web/icons.png";
	}

	public CRPropertyDescriptor[] supportedDescriptor() throws IntrospectionException {
		return ArrayUtils.addAll(super.supportedDescriptor(), getCrp());
	}

	protected JComponent initEditor() {
		PicturesWidget localPicturesWidget = (PicturesWidget) this.data;
		if (this.editor == null) {
			this.editor = FRGUIPaneFactory.createBorderLayout_S_Pane();
			UITextField localUITextField = new UITextField(5);
			localUITextField.setOpaque(false);
			UILabel localUILabel = new UILabel();
			localUILabel.setHorizontalAlignment(0);
			localUILabel.setText("PICS GOGO");
			this.editor.add(localUILabel);
			this.editor.setBackground(Color.gray);
		}
		return this.editor;
	}

	public boolean canEnterIntoParaPane() {
		return false;
	}

	protected CRPropertyDescriptor[] getCrp() throws IntrospectionException {
		return new CRPropertyDescriptor[]{creatNonListenerStyle(0),creatNonListenerStyle(1),creatNonListenerStyle(2),creatNonListenerStyle(3),creatNonListenerStyle(4)};
	}

	protected CRPropertyDescriptor creatNonListenerStyle(int i) throws IntrospectionException {
		CRPropertyDescriptor[] arrayOfCRPropertyDescriptor = {
				new CRPropertyDescriptor("param0", this.data.getClass()).setI18NName("数据库连接").putKeyValue("category", "Advanced").setEditorClass(ShortCutTextEditor.class),
				new CRPropertyDescriptor("param1", this.data.getClass()).setI18NName("用户名").putKeyValue("category", "Advanced").setEditorClass(ShortCutTextEditor.class),
				new CRPropertyDescriptor("param2", this.data.getClass()).setI18NName("密码").putKeyValue("category", "Advanced").setEditorClass(ShortCutTextEditor.class),
				new CRPropertyDescriptor("param3", this.data.getClass()).setI18NName("查询sql").putKeyValue("category", "Advanced").setEditorClass(ShortCutTextEditor.class),
				new CRPropertyDescriptor("param4", this.data.getClass()).setI18NName("所在项目id").putKeyValue("category", "Advanced").setEditorClass(ShortCutTextEditor.class)};
		return arrayOfCRPropertyDescriptor[i];
	}
}
