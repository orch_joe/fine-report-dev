package com.fr.plugin.zhjoe.pics.ui;

import com.fr.design.mainframe.widget.accessibles.UneditableAccessibleEditor;
import com.fr.report.cell.FloatElement;

public class AccessiblePicturesModelEditor extends UneditableAccessibleEditor {
	private PicturesWidget pic;

	public AccessiblePicturesModelEditor(PicturesWidget paramPicturesWidget) {
		super(new PicturesModelWrapper());
		this.pic = paramPicturesWidget;
	}

	public FloatElement getValue() {
		return (FloatElement) super.getValue();
	}
}
