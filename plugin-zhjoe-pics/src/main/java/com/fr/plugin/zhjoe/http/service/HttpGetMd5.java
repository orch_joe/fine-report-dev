package com.fr.plugin.zhjoe.http.service;

import com.fr.decision.fun.impl.BaseHttpHandler;
import com.fr.plugin.zhjoe.util.MD5Util;
import com.fr.plugin.zhjoe.util.StringUtil;
import com.fr.third.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

//http://localhost:8075/webroot/decision/plugin/public/com.fr.plugin.zhjoe/getMd5?code=123456

public class HttpGetMd5 extends BaseHttpHandler {
	@Override
	public RequestMethod getMethod() {
		return RequestMethod.GET;
	}

	@Override
	public String getPath() {
		return "/getMd5";
	}

	@Override
	public boolean isPublic() {
		return true;
	}

	@Override
	public void handle(HttpServletRequest req, HttpServletResponse res) throws Exception {
		String code = req.getParameter("code");
		//res.setContentType("application/text;charset=UTF-8");
		//res.setCharacterEncoding("UTF-8");
		PrintWriter out = res.getWriter();
		if (StringUtil.isEmpty(code)) {
			out.println("错误");
		} else {
			out.println(MD5Util.MD5(code));
		}
		out.flush();
		out.close();
	}
}
