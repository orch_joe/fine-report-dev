package com.fr.plugin.zhjoe.pics.ui;

import com.alibaba.fastjson.JSON;
import com.fr.base.FRContext;
import com.fr.base.Formula;
import com.fr.base.TemplateUtils;
import com.fr.form.ui.Widget;
import com.fr.general.GeneralUtils;
import com.fr.json.JSONException;
import com.fr.json.JSONObject;
import com.fr.plugin.transform.ExecuteFunctionRecord;
import com.fr.plugin.transform.FunctionRecorder;
import com.fr.plugin.zhjoe.db.ProjectDbUtil;
import com.fr.plugin.zhjoe.tools.SessionUtil;
import com.fr.plugin.zhjoe.util.StringUtil;
import com.fr.script.Calculator;
import com.fr.security.JwtUtils;
import com.fr.stable.StableUtils;
import com.fr.stable.UtilEvalError;
import com.fr.stable.core.NodeVisitor;
import com.fr.stable.web.Repository;
import com.fr.stable.web.Session;
import com.fr.stable.web.SessionInfo;
import com.fr.stable.web.SessionProvider;
import com.fr.stable.xml.XMLPrintWriter;
import com.fr.stable.xml.XMLableReader;
import com.fr.third.guava.collect.Lists;
import com.fr.web.session.SessionLocalManager;
import com.fr.web.session.SessionReferrerContext;
import com.fr.workspace.WorkContext;
import org.apache.catalina.manager.util.SessionUtils;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

@FunctionRecorder
public class PicturesWidget extends Widget {
	public List<Map<String, Object>> urls = new ArrayList();
	public String param0;
	public String param1;
	public String param2;
	public String param3;
	public String param4;


	public String getXType() {
		return "pics";
	}

	public boolean isEditor() {
		return false;
	}

	public String[] supportedEvents() {
		return new String[]{"afterinit", "click"};
	}

	@ExecuteFunctionRecord
	public JSONObject createJSONConfig(Repository paramRepository, Calculator paramCalculator, NodeVisitor paramNodeVisitor) throws JSONException {
		JSONObject localJSONObject = super.createJSONConfig(paramRepository, paramCalculator, paramNodeVisitor);
		String str1 = WorkContext.getCurrent().getPath();
		String str2 = System.getProperty("os.name");
		if (!Pattern.matches("Linux.*", str2)) {
			String[] arrayOfString;
			if (Pattern.matches("Windows.*", str2)) {
				arrayOfString = str1.split("\\\\");
				FileUtils.path = arrayOfString[(arrayOfString.length - 2)];
			} else if (Pattern.matches("Mac.*", str2)) {
				arrayOfString = str1.split("/");
				FileUtils.path = arrayOfString[(arrayOfString.length - 2)];
			}
		}

		List<Map<String, Object>> results = Lists.newArrayList();
		String sql = "";
		if (param3.trim().equalsIgnoreCase("1")) {
			sql = buildProjectSql(paramRepository);
		} else {
			sql = param3;
		}
		if (StringUtil.isEmpty(sql) || sql.equals("无法获取用户信息")) {
			results = Lists.newArrayList();
		} else {
			results = ProjectDbUtil.getProject(param0, param1, param2, sql);
		}
		urls.clear();
		for (Map<String, Object> url : results) {
			url.put("id", url.get("id"));
			url.put("url", url.get("url"));
			url.put("msg", url.get("msg"));
			urls.add(url);
		}
		localJSONObject.put("urls", urls);
		localJSONObject.put("contentPath", FileUtils.path);
		//添加进去查询sql
		localJSONObject.put("sql", sql);
		return localJSONObject;
	}

	private String buildProjectSql(Repository paramRepository) {
		String projectName = "";
		SessionProvider userInfo = SessionUtil.getSessionProvider(paramRepository.getSessionID());
		if (null == userInfo) {
			return "无法获取用户信息";
		}
		List<String> roles;
		try {
			roles = Lists.newArrayList(userInfo.getWebContext().getUserRole().split(","));
		} catch (Exception e) {
			e.printStackTrace();
			roles = Lists.newArrayList();
		}
		if (roles.isEmpty()) {
			projectName = "''";
		} else {
			List<String> allRoles = Lists.newArrayList();
			for (String role : roles) {
				allRoles.addAll(Lists.newArrayList(role.split("-")));
			}
			if (allRoles.isEmpty()) {
			} else {
				for (String allRole : allRoles) {
					if (StringUtil.isEmpty(projectName)) {
						projectName = "'" + allRole + "'";
					} else {
						projectName = projectName + "," + "'" + allRole + "'";
					}
				}
			}
		}
		StringBuffer sb = new StringBuffer();
		sb.append("SELECT");
		sb.append("	project_id,");
		sb.append("	image_path,");
		sb.append("	text_value");
		sb.append("FROM");
		sb.append("	project_report_view");
		sb.append("WHERE");
		sb.append("PROJECT_ID IN ( SELECT id FROM T_PROJECT WHERE SHORT_NAME IN (" + projectName + ") )");
		return sb.toString();
	}

	public void writeXML(XMLPrintWriter paramXMLPrintWriter) {
		super.writeXML(paramXMLPrintWriter);
		paramXMLPrintWriter.startTAG("com.fr.plugin.zhjoe.pics");
		paramXMLPrintWriter.end();
		paramXMLPrintWriter.startTAG("param0").attr("param0", StringUtil.getDefaultValue(this.param0)).attr("param1", StringUtil.getDefaultValue(this.param1)).attr("param2", StringUtil.getDefaultValue(this.param2)).attr("param3", StringUtil.getDefaultValue(this.param3)).attr("param4", StringUtil.getDefaultValue(this.param4)).end();
	}

	public String getValue(Repository paramRepository, Calculator paramCalculator, String value) {
		System.out.println("计算的value为:" + value);
		if (StableUtils.canBeFormula(value)) {
			System.out.println("可以计算:" + value);
			try {
				return GeneralUtils.objectToString(paramCalculator.eval(new Formula(value)));
			} catch (UtilEvalError localUtilEvalError) {
				FRContext.getLogger().error(localUtilEvalError.getMessage(), localUtilEvalError);
			}
		} else {
			System.out.println("不可以计算:"+value);
			try {
				return TemplateUtils.render(value, paramCalculator);
			} catch (Exception localException) {
				FRContext.getLogger().error(localException.getMessage(), localException);
			}
		}
		return value;
	}

	public void readXML(XMLableReader paramXMLableReader) {
		super.readXML(paramXMLableReader);
		if (paramXMLableReader.isChildNode()) {
			this.param0 = paramXMLableReader.getAttrAsString("param0", "");
			this.param1 = paramXMLableReader.getAttrAsString("param1", "");
			this.param2 = paramXMLableReader.getAttrAsString("param2", "");
			this.param3 = paramXMLableReader.getAttrAsString("param3", "");
			this.param4 = paramXMLableReader.getAttrAsString("param4", "");
		}
	}

	public List<Map<String, Object>> getUrls() {
		return urls;
	}

	public void setUrls(List<Map<String, Object>> urls) {
		this.urls = urls;
	}

	public String getParam0() {
		return param0;
	}

	public void setParam0(String param0) {
		this.param0 = param0;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getParam3() {
		return param3;
	}

	public void setParam3(String param3) {
		this.param3 = param3;
	}

	public String getParam4() {
		return param4;
	}

	public void setParam4(String param4) {
		this.param4 = param4;
	}
}
