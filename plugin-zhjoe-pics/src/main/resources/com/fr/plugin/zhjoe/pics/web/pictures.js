/**
 * Created by orchjoe on 16/2/25.
 */
;(function ($) {
    FR.pics = FR.extend(FR.BaseEditor, {
        _defaultConfig: function () {
            return $.extend(FR.pics.superclass._defaultConfig.apply(), {
                baseName: 'sl.pictures',
                baseClass: 'sl.pictures',
                widgetName: "",
                src: "",
                width: "100%",
                height: "100%",
                showOverFlowX: true,
                showOverFlowY: true
            });
        },
        _init: function () {
            FR.pics.superclass._init.apply(this, arguments);
            if (typeof paramm == 'undefined') {
                paramm = {};
            }
            let o = this.options;
            let projectPath = o.contentPath;
            let path = "/" + projectPath + "/ReportServer?op=resource&resource=/com/fr/plugin/zhjoe/pics/web/";
            let pics = "";
            for (var i = 0; i < o.urls.length; i++) {
                let d = o.urls[i];
                pics = pics + "<div class='swiper-slide' onclick=window.open('" + d.id + "')>" +
                    "               <img src='" + d.url + "' />" +
                    "               <span>" + d.msg + "</span>" +
                    "          </div>";
            }


            let html = "" +
                "<div class='swiper-container'>" +
                "    <div class='swiper-wrapper'>" +
                pics +
                "    </div>" +
                "    <div class='swiper-pagination'></div>" +
                "    <div class='swiper-button-next'></div>" +
                "    <div class='swiper-button-prev'></div>" +
                "</div>";

            console.log("调试信息2", o);
            setTimeout(function () {
                $("[widgetname=" + o.widgetName.toUpperCase() + "]").html(html);
                var swiper = new Swiper('.swiper-container', {
                    spaceBetween: 30,
                    centeredSlides: true,
                    autoplay: {
                        delay: 2500,
                        disableOnInteraction: false,
                    },
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true,
                    },
                    navigation: {
                        nextEl: '.swiper-button-next',
                        prevEl: '.swiper-button-prev',
                    },
                });
            }, 10);

        }
    });

    $.shortcut('pics', FR.pics);
})(jQuery);

