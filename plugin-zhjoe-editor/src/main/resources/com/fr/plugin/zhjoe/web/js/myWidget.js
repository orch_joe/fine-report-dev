/**
 * Created by orchjoe on 16/2/25.
 */
;(function ($) {
    FR.Editor = FR.extend(FR.EditCompBaseEditor, {
        _defaultConfig: function () {
            return $.extend(FR.Editor.superclass._defaultConfig.apply(), {
                baseName: 'zxl.ue',
                baseClass: 'zxlue',
                widgetName: "",
                src: "",
                width: "100%",
                height: "100%",
            });
        },
        _hideView: function () {
            this.element.show();
        },
        _init: function () {
            FR.Editor.superclass._init.apply(this, arguments);
            debugger
            this.initData();
            this.initHeight();
            var realb1 = $("td[editor='" + this.options.location + "']");
            if (!realb1[0]) {
                realb1 = $("td#" + this.options.location + "-0-0");
            }
            var realb2 = realb1.parent();
            $(realb2).height(this.options.height)
            realb1.width(this.options.width);
            realb1.height(this.options.height);
            realb1.attr("showashtml", "true");
            realb1.attr("widgetheight", this.options.height)
            // 控件属性
            var o = this.options;
            var that = this;
            this.editComp = this.element;
            if (!jQuery.support.boxModel) {
                this.editComp.css({
                    "white-space": "normal"
                })
            }
            this.editComp.attr("showashtml", "true");
            var E = window.wangEditor;
            var editor = new E(that.element[0]);
            editor.customConfig.menus = [
                'head',  // 标题
                'bold',  // 粗体
                'fontSize',  // 字号
                'fontName',  // 字体
                'italic',  // 斜体
                'underline',  // 下划线
                'strikeThrough',  // 删除线
                'foreColor',  // 文字颜色
                'backColor',  // 背景颜色
                'link',  // 插入链接
                'list',  // 列表
                'justify',  // 对齐方式
                'quote',  // 引用
                // 'emoticon',  // 表情
                'image',  // 插入图片
                'table',  // 表格
                //'video',  // 插入视频
                'code',  // 插入代码
                'undo',  // 撤销
                'redo'  // 重复
            ];
            editor.customConfig.uploadImgShowBase64 = true;
            editor.customConfig.onfocus = function () {
                that.editing = true;
            };
            editor.customConfig.onblur = function (html) {
                that.editing = false;
            };
            editor.customConfig.debug = true;
            editor.customConfig.lang = {
                '设置标题': FR.i18n.wang_editor_bar_title,
                '正文': FR.i18n.wang_editor_bar_p,
                '链接文字': FR.i18n.wang_editor_bar_link,
                '上传图片': FR.i18n.wang_editor_bar_upload_image,
                '创建': FR.i18n.wang_editor_bar_init,
                "视频": FR.i18n.wang_editor_bar_video,
                "有序列表": FR.i18n.wang_editor_bar_on_list,
                "无序列表": FR.i18n.wang_editor_bar_un_list,
                "文字颜色": FR.i18n.wang_editor_bar_text_color,
                "表格": FR.i18n.wang_editor_bar_table,
                "靠左": FR.i18n.wang_editor_bar_left,
                "靠右": FR.i18n.wang_editor_bar_right,
                "居中": FR.i18n.wang_editor_bar_center,
                "行": FR.i18n.wang_editor_bar_row,
                "插入": FR.i18n.wang_editor_bar_insert,
                "表情": FR.i18n.wang_editor_bar_emoji,
                "手势": FR.i18n.wang_editor_bar_gesture,
                "的": FR.i18n.wang_editor_bar_de,
                "背景色": FR.i18n.wang_editor_bar_bg_color,
                "对齐方式": FR.i18n.wang_editor_bar_alignment,
                "网络图片": FR.i18n.wang_editor_bar_image,
                // 还可自定添加更多
            }
            editor.create();
            editor.$textElem.on('keydown', function (e) {
                e.stopPropagation();
            });
            editor.txt.html(o.value);
            that.editor = editor;
            var h = this.options.height;
            if (h && h !== "auto") {
                $(that.editor.$textElem[0]).parent().css("height", h - 25);
            }
            that.editComp.on("keydown", function (event) {
                console.info("press key " + event.key);
                if (that.editing && event.key === "Enter") {
                    event = event || window.event;  //用于IE
                    if (event.preventDefault) event.preventDefault();  //标准技术
                    if (event.returnValue) event.returnValue = false;  //IE
                    // return false;   //用于处理使用对象属性注册的处理程序
                    event.stopPropagation();
                }
            })
            // c.ready(function () {
            //     var E = window.wangEditor;
            //     var editor = new E('#'+scrp)
            //     // 或者 var editor = new E( document.getElementById('editor') )
            //     editor.create()
            // })

        },
        /**
         * @param {String} v 新的地址
         */
        setValue: function (v) {
            if (this.editor) {
                this.editor.txt.html(v)
            }
        },
        startEditing: function () {
            this.element.show();
        },
        stopEditing: function () {
            this.element.show();
        },
        /**
         * 获取网页框的src地址
         * @returns {String} 地址
         */
        getValue: function () {
            if (this.editor) {
                var t = this.editor.txt.html()
                if (t && t.indexOf("class=\"wang_e\"") != -1) {
                    return t;
                }
                return "<div class='wang_e'>" + t + "</div>"
            }
        },
        height: function () {
            return this.options.height;
        },
        initHeight: function () {
            var cssHeight = 500;
            if (this.options.height > cssHeight) {
                this.options.height = cssHeight;
            }
        },
        setEnable: function (enable) {
            FR.Editor.superclass.setEnable.apply(this, arguments);
            if (enable) {
                this.element.show();
            } else {
                this.element.hide();
            }
        },
        doResize: function (give) {
            FR.Editor.superclass.doResize.apply(this, arguments);
        },
        setVisible: function (r) {
            if (r) {
                this.element.show();
            } else {
                this.element.hide();
            }
        },
        reset: function () {

        }
    });
    $.shortcut('zhjoeditor', FR.Editor);
})(jQuery);

