package com.fr.plugin.zhjoe.panes;

import com.fr.base.BaseUtils;
import com.fr.design.actions.UpdateAction;
import com.fr.design.dialog.BasicDialog;
import com.fr.design.dialog.BasicPane;
import com.fr.design.dialog.DialogActionAdapter;
import com.fr.design.gui.icontainer.UIScrollPane;
import com.fr.design.gui.itoolbar.UIToolbar;
import com.fr.design.javascript.ListenerEditPane;
import com.fr.design.layout.FRGUIPaneFactory;
import com.fr.design.menu.ShortCut;
import com.fr.design.menu.ToolBarDef;
import com.fr.design.utils.gui.GUICoreUtils;
import com.fr.design.widget.EventCreator;
import com.fr.form.event.Listener;
import com.fr.general.Inter;
import com.fr.js.JavaScriptImpl;
import com.fr.report.web.WebContent;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListCellRenderer;
import javax.swing.SwingUtilities;

public class MyEventPane extends BasicPane {
	private DefaultListModel listModel;
	private JList eventList;
	private AddBuildingListener addAction;
	private AddStagesListener stagesAddAction;
	private AddAreaListener areaAddAction;
	private EditAction editAction;
	private RemoveAction removeAction;
	private int itemHeight = 20;
	public static ListCellRenderer render = new DefaultListCellRenderer() {
		public Component getListCellRendererComponent(JList paramAnonymousJList, Object paramAnonymousObject, int paramAnonymousInt, boolean paramAnonymousBoolean1, boolean paramAnonymousBoolean2) {
			super.getListCellRendererComponent(paramAnonymousJList, paramAnonymousObject, paramAnonymousInt, paramAnonymousBoolean1, paramAnonymousBoolean2);
			if ((paramAnonymousObject instanceof Listener)) {
				Listener localListener = (Listener) paramAnonymousObject;
				setText(EventCreator.switchLang(localListener.getEventName()));
			}
			return this;
		}
	};
	public MouseListener editListener = new MouseAdapter() {
		public void mouseReleased(MouseEvent paramAnonymousMouseEvent) {
			if (MyEventPane.this.eventList.getSelectedIndex() < 0)
				return;
			MyEventPane.this.checkEnableState();
			if ((paramAnonymousMouseEvent.getClickCount() >= 2) && (SwingUtilities.isLeftMouseButton(paramAnonymousMouseEvent)))
				MyEventPane.this.edit();
			if (SwingUtilities.isRightMouseButton(paramAnonymousMouseEvent)) {
				int i = paramAnonymousMouseEvent.getY();
				MyEventPane.this.eventList.setSelectedIndex((int) Math.floor(i / MyEventPane.this.itemHeight));
				JPopupMenu localJPopupMenu = new JPopupMenu();
				localJPopupMenu.add(MyEventPane.this.editAction);
				localJPopupMenu.add(MyEventPane.this.removeAction);
				GUICoreUtils.showPopupMenu(localJPopupMenu, MyEventPane.this.eventList, paramAnonymousMouseEvent.getX() - 1, paramAnonymousMouseEvent.getY() - 1);
			}
		}
	};

	public MyEventPane() {
		initComponents();
	}

	private void initComponents() {
		setLayout(FRGUIPaneFactory.createBorderLayout());
		this.listModel = new DefaultListModel();
		this.eventList = new JList(this.listModel);
		this.eventList.setCellRenderer(render);
		this.eventList.addMouseListener(this.editListener);
		add(new UIScrollPane(this.eventList), "Center");
		this.addAction = new AddBuildingListener();
		this.stagesAddAction = new AddStagesListener();
		this.areaAddAction = new AddAreaListener();
		this.editAction = new EditAction();
		this.removeAction = new RemoveAction();
		ToolBarDef localToolBarDef = new ToolBarDef();
		localToolBarDef.addShortCut(new ShortCut[]{this.addAction});
		localToolBarDef.addShortCut(new ShortCut[]{this.stagesAddAction});
		localToolBarDef.addShortCut(new ShortCut[]{this.areaAddAction});
		localToolBarDef.addShortCut(new ShortCut[]{this.editAction});
		localToolBarDef.addShortCut(new ShortCut[]{this.removeAction});
		UIToolbar localUIToolbar = ToolBarDef.createJToolBar();
		localToolBarDef.updateToolBar(localUIToolbar);
		localUIToolbar.setPreferredSize(new Dimension(localUIToolbar.getWidth(), 26));
		add(localUIToolbar, "North");
	}

	public void setEnabled(boolean paramBoolean) {
		super.setEnabled(paramBoolean);
		this.eventList.setEnabled(paramBoolean);
		this.addAction.setEnabled(paramBoolean);
		this.editAction.setEnabled(paramBoolean);
		this.removeAction.setEnabled(paramBoolean);
		checkEnableState();
	}

	private void checkEnableState() {
		if ((this.listModel.size() == 0) || (this.eventList.getSelectedIndex() < 0))
			setEditEnabled(false);
		else
			setEditEnabled(true);
	}

	private void setEditEnabled(boolean paramBoolean) {
		this.editAction.setEnabled(paramBoolean);
		this.removeAction.setEnabled(paramBoolean);
	}

	protected String title4PopupWindow() {
		return Inter.getLocText("Event_Set");
	}

	public void populate(List<Listener> paramList) {
		this.listModel.removeAllElements();
		for (int i = 0; i < paramList.size(); i++)
			this.listModel.addElement(paramList.get(i));
		this.eventList.validate();
	}

	public List<Listener> update() {
		ArrayList localArrayList = new ArrayList();
		for (int i = 0; i < this.listModel.getSize(); i++)
			localArrayList.add((Listener) this.listModel.get(i));
		return localArrayList;
	}

	public void edit() {
		final int i = this.eventList.getSelectedIndex();
		if (!(this.listModel.getElementAt(i) instanceof Listener))
			return;
		Listener localListener = (Listener) this.listModel.getElementAt(i);
		final ListenerEditPane localListenerEditPane = new ListenerEditPane(WebContent.getDefaultArg(localListener.getEventName()));
		localListenerEditPane.populateBean(localListener);
		BasicDialog localBasicDialog = localListenerEditPane.showWindow(SwingUtilities.getWindowAncestor(this));
		localBasicDialog.addDialogActionListener(new DialogActionAdapter() {
			public void doOk() {
				MyEventPane.this.listModel.setElementAt(localListenerEditPane.updateBean(), i);
				MyEventPane.this.eventList.validate();
			}
		});
		localBasicDialog.setVisible(true);
	}

	public void addEventa(String paramString) {
		final ListenerEditPane localListenerEditPane = new ListenerEditPane(WebContent.getDefaultArg(paramString));
		Listener localListener = new Listener(paramString, new JavaScriptImpl());
		localListenerEditPane.populateBean(localListener);
		BasicDialog localBasicDialog = localListenerEditPane.showWindow(SwingUtilities.getWindowAncestor(this));
		localBasicDialog.addDialogActionListener(new DialogActionAdapter() {
			public void doOk() {
				MyEventPane.this.listModel.addElement(localListenerEditPane.updateBean());
				MyEventPane.this.eventList.validate();
			}
		});
		localBasicDialog.setVisible(true);
	}

	public class AddStagesListener extends UpdateAction {
		public AddStagesListener() {
			setName("添分期点击事件");
			setSmallIcon(BaseUtils.readIcon("/com/fr/design/images/buttonicon/add.png"));
		}

		public void actionPerformed(ActionEvent paramActionEvent) {
			MyEventPane.this.addEventa("click_point_fenqi");
		}
	}

	public class AddAreaListener extends UpdateAction {
		public AddAreaListener() {
			setName("添加地块事件");
			setSmallIcon(BaseUtils.readIcon("/com/fr/design/images/buttonicon/add.png"));
		}

		public void actionPerformed(ActionEvent paramActionEvent) {
			MyEventPane.this.addEventa("click_point_quyu");
		}
	}

	public class AddBuildingListener extends UpdateAction {
		public AddBuildingListener() {
			setName("添加楼栋事件");
			setSmallIcon(BaseUtils.readIcon("/com/fr/design/images/buttonicon/add.png"));
		}

		public void actionPerformed(ActionEvent paramActionEvent) {
			MyEventPane.this.addEventa("click_point_loudong");
		}
	}

	public class EditAction extends UpdateAction {
		public EditAction() {
			setName(Inter.getLocText("Edit"));
			setSmallIcon(BaseUtils.readIcon("/com/fr/design/images/control/edit.png"));
		}

		public void actionPerformed(ActionEvent paramActionEvent) {
			if (MyEventPane.this.eventList.getSelectedIndex() < 0)
				return;
			MyEventPane.this.edit();
		}
	}

	public class RemoveAction extends UpdateAction {
		public RemoveAction() {
			setName(Inter.getLocText("Delete"));
			setSmallIcon(BaseUtils.readIcon("/com/fr/base/images/cell/control/remove.png"));
		}

		public void actionPerformed(ActionEvent paramActionEvent) {
			int i = MyEventPane.this.eventList.getSelectedIndex();
			if ((i < 0) || (!(MyEventPane.this.listModel.getElementAt(i) instanceof Listener)))
				return;
			int j = JOptionPane.showConfirmDialog(MyEventPane.this, Inter.getLocText("Are_You_Sure_To_Delete_The_Data") + "?", "Message", 0);
			if (j != 0)
				return;
			MyEventPane.this.listModel.removeElementAt(i);
			MyEventPane.this.checkEnableState();
			MyEventPane.this.eventList.validate();
		}
	}
}
