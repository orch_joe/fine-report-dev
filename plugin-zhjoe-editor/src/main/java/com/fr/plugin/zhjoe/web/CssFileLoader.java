package com.fr.plugin.zhjoe.web;

import com.fr.stable.fun.Authorize;
import com.fr.stable.fun.impl.AbstractCssFileHandler;
@Authorize(callSignKey="com.zhjoe.editor")
public class CssFileLoader extends AbstractCssFileHandler {
	public String[] pathsForFiles() {
		return new String[]{"/com/fr/plugin/zhjoe/web/css/myChart.css"};
	}
}
