package com.fr.plugin.zhjoe.providers;

import com.fr.design.beans.BasicBeanPane;
import com.fr.design.fun.impl.AbstractCellWidgetOptionProvider;
import com.fr.form.ui.Widget;
import com.fr.plugin.transform.FunctionRecorder;
import com.fr.plugin.zhjoe.panes.rte.MyCellPane;
import com.fr.plugin.zhjoe.widgets.MyWidget;
import com.fr.stable.fun.mark.API;

@API(level = 1)
@FunctionRecorder
public class MyParameterCellOptionProvider extends AbstractCellWidgetOptionProvider {
	public Class<? extends Widget> classForWidget() {
		return MyWidget.class;
	}

	public String iconPathForWidget() {
		return "/com/fr/plugin/zhjoe/web/images/icon.png";
	}

	public String nameForWidget() {
		return "zhjoe富文本编辑器";
	}

	public Class<? extends BasicBeanPane<? extends Widget>> appearanceForWidget() {
		return MyCellPane.class;
	}
}
