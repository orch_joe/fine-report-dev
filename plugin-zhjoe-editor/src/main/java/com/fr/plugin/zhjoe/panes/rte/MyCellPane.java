package com.fr.plugin.zhjoe.panes.rte;


import com.fr.design.widget.ui.AbstractDataModify;
import com.fr.plugin.zhjoe.widgets.MyWidget;

public class MyCellPane extends AbstractDataModify<MyWidget> {
	private MyWidget widget;

	public void populateBean(MyWidget paramMyWidget) {
		this.widget = paramMyWidget;
	}

	public MyWidget updateBean() {
		return this.widget;
	}

	protected String title4PopupWindow() {
		return "WangEditor富文本编辑器";
	}
}
