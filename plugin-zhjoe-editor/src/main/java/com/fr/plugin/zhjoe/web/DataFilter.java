package com.fr.plugin.zhjoe.web;

import com.fr.json.JSONObject;

public abstract interface DataFilter {
	public abstract boolean doFilter(JSONObject paramJSONObject);
}
