package com.fr.plugin.zhjoe.web;

import com.fr.plugin.transform.ExecuteFunctionRecord;
import com.fr.plugin.transform.FunctionRecorder;
import com.fr.stable.fun.impl.AbstractJavaScriptFileHandler;

@FunctionRecorder(localeKey = "zhjoeditor")
public class JSFileLoader extends AbstractJavaScriptFileHandler {
	@ExecuteFunctionRecord
	public String[] pathsForFiles() {
		return new String[]{"/com/fr/plugin/zhjoe/web/js/myWidget.js", "/com/fr/plugin/zhjoe/web/js/wangeditor.js"};
	}

	public String encode() {
		return "UTF-8";
	}
}
