package com.fr.plugin.zhjoe.panes.rte;

import com.fr.design.designer.creator.CRPropertyDescriptor;
import com.fr.design.designer.creator.XWidgetCreator;
import com.fr.design.gui.ilable.UILabel;
import com.fr.design.mainframe.widget.editors.RegexEditor;
import com.fr.design.mainframe.widget.editors.RegexEditor.RegexEditor4TextArea;
import com.fr.design.mainframe.widget.editors.WidgetValueEditor;
import com.fr.design.mainframe.widget.renderer.RegexCellRencerer;
import com.fr.form.ui.TextEditor;
import com.fr.form.ui.Widget;
import com.fr.form.ui.reg.RegExp;
import com.fr.general.Inter;
import com.fr.stable.ArrayUtils;
import com.fr.stable.StringUtils;

import java.awt.Color;
import java.awt.Dimension;
import java.beans.IntrospectionException;
import javax.swing.JComponent;

public class WidgetPane extends XWidgetCreator {
	public WidgetPane(Widget paramWidget, Dimension paramDimension) {
		super(paramWidget, paramDimension);
	}

	protected String getIconName() {
		return "text_pane_16.png";
	}

	protected JComponent initEditor() {
		UILabel localUILabel = new UILabel("富文本编辑器");
		localUILabel.setForeground(Color.BLUE);
		localUILabel.setVerticalAlignment(0);
		localUILabel.setHorizontalAlignment(0);
		setBorder(DEFALUTBORDER);
		return this.editor = localUILabel;
	}

	public CRPropertyDescriptor[] supportedDescriptor() throws IntrospectionException {
		CRPropertyDescriptor[] arrayOfCRPropertyDescriptor = (CRPropertyDescriptor[]) ArrayUtils.addAll(new CRPropertyDescriptor[]{new CRPropertyDescriptor("widgetValue", this.data.getClass()).setI18NName(Inter.getLocText("FR-Designer-Estate_Widget_Value")).setEditorClass(WidgetValueEditor.class).putKeyValue("category", "FR-Designer_Advanced")}, super.supportedDescriptor());
		CRPropertyDescriptor localCRPropertyDescriptor1 = new CRPropertyDescriptor("regex", this.data.getClass()).setI18NName(Inter.getLocText("FR-Designer_Input_Rule")).setEditorClass(RegexEditor.RegexEditor4TextArea.class).putKeyValue("renderer", RegexCellRencerer.class).putKeyValue("FR-Designer_Validate", "FR-Designer_Validate");
		CRPropertyDescriptor localCRPropertyDescriptor2 = new CRPropertyDescriptor("regErrorMessage", this.data.getClass()).setI18NName(Inter.getLocText("FR-Engine_Verify-Message")).putKeyValue("FR-Designer_Validate", "FR-Designer_Validate");
		CRPropertyDescriptor localCRPropertyDescriptor3 = new CRPropertyDescriptor("waterMark", this.data.getClass()).setI18NName(Inter.getLocText("FR-Designer_WaterMark")).putKeyValue("category", "FR-Designer_Advanced");
		boolean bool = true;
		bool = isDisplayRegField(bool);
		return bool ? (CRPropertyDescriptor[]) ArrayUtils.addAll(arrayOfCRPropertyDescriptor, new CRPropertyDescriptor[]{localCRPropertyDescriptor1, localCRPropertyDescriptor2, localCRPropertyDescriptor3}) : (CRPropertyDescriptor[]) ArrayUtils.addAll(arrayOfCRPropertyDescriptor, new CRPropertyDescriptor[]{localCRPropertyDescriptor1, localCRPropertyDescriptor3});
	}

	public boolean isDisplayRegField(boolean paramBoolean) {
		RegExp localRegExp = ((TextEditor) toData()).getRegex();
		if ((localRegExp == null) || (!StringUtils.isNotEmpty(localRegExp.toRegText())))
			paramBoolean = false;
		return paramBoolean;
	}
}
