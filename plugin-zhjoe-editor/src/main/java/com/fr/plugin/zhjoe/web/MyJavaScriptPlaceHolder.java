package com.fr.plugin.zhjoe.web;

import com.fr.stable.fun.JavaScriptPlaceHolder;
import com.fr.stable.fun.JavaScriptPlaceHolder.ScriptTag;
import com.fr.stable.fun.impl.AbstractJavaScriptPlaceHolder;

public class MyJavaScriptPlaceHolder extends AbstractJavaScriptPlaceHolder {
	private static JavaScriptPlaceHolder.ScriptTag[] SCRIPTS = {JavaScriptPlaceHolder.ScriptTag.build().src("https://webapi.amap.com/maps?v=1.4.13&key=76f2ac16b46d20c24d7223885ac39fed&plugin=AMap.MouseTool,AMap.ToolBar"), JavaScriptPlaceHolder.ScriptTag.build().src("https://webapi.amap.com/ui/1.0/main.js"),
			JavaScriptPlaceHolder.ScriptTag.build().src("https://webapi.amap.com/ui/1.0/ui/overlay/SimpleMarker.js")};

	public JavaScriptPlaceHolder.ScriptTag[] holderScripts() {
		return SCRIPTS;
	}
}
