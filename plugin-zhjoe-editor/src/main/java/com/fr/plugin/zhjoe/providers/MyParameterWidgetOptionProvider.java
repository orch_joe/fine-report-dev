package com.fr.plugin.zhjoe.providers;

import com.fr.design.fun.impl.AbstractParameterWidgetOptionProvider;
import com.fr.form.ui.Widget;
import com.fr.plugin.transform.FunctionRecorder;
import com.fr.plugin.zhjoe.panes.rte.WidgetPane;
import com.fr.plugin.zhjoe.widgets.MyWidget;

@FunctionRecorder
public class MyParameterWidgetOptionProvider extends AbstractParameterWidgetOptionProvider {
	public Class<? extends Widget> classForWidget() {
		return MyWidget.class;
	}

	public Class<?> appearanceForWidget() {
		return WidgetPane.class;
	}

	public String iconPathForWidget() {
		return "/com/fr/plugin/zhjoe/web/images/icon.png";
	}

	public String nameForWidget() {
		return "zhjoe富文本编辑器";
	}
}
