package com.fr.plugin.zhjoe.widgets;

import com.fr.form.ui.Interactive;
import com.fr.form.ui.TextEditor;
import com.fr.json.JSONException;
import com.fr.json.JSONObject;
import com.fr.script.Calculator;
import com.fr.stable.core.NodeVisitor;
import com.fr.stable.fun.FunctionHelper;
import com.fr.stable.fun.FunctionProcessor;
import com.fr.stable.fun.impl.AbstractFunctionProcessor;
import com.fr.stable.web.Repository;

import javax.servlet.http.HttpServletRequest;

public class MyWidget extends TextEditor implements Interactive {
	public void mixinReturnData(HttpServletRequest paramHttpServletRequest, Calculator paramCalculator, JSONObject paramJSONObject) throws JSONException {
	}

	public JSONObject createJSONConfig(Repository paramRepository, Calculator paramCalculator, NodeVisitor paramNodeVisitor) throws JSONException {
		JSONObject localJSONObject = super.createJSONConfig(paramRepository, paramCalculator, paramNodeVisitor);
		localJSONObject.put("multiline", true);
		return localJSONObject;
	}

	public String getXType() {
		return "zhjoeditor";
	}
}
